open import Categories
open import lamInd
open import Library

module LCalcToCat where

lambdaCat : Cat
lambdaCat = record
              { Obj = Ty
              ; Hom = λ A B → Tm ε (A ⇒ B)
              ; iden = Λ (var vz)
              ; _∙_ = λ c b → Λ ((wkTm vz c) ∙ ((wkTm vz b) ∙ (var vz)))
              ; idl = {!!}
              ; idr = {!!}
              ; ass = {!!}
              }
