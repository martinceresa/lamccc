open import Data.Vec
open import Data.Fin hiding (_+_)
open import Data.Fin.Properties
open import Data.Nat
open import Relation.Binary.PropositionalEquality
open import Relation.Nullary.Decidable as Dec

module LambdaCalc where

 module GoBack where
  -- | Tipos
 infixr 30 _⇒_
 data Type : Set where
    -- | Unit type
    ₁ : Type
    -- | Function Type, A -> B
    _⇒_ : Type → Type → Type
    -- | Product Type, A x B
    _X_ : Type → Type → Type

 Ctx : ℕ → Set
 Ctx = Vec Type


 data Term {n : ℕ} : Set where
  -- | Variables. Usamos fin para no irnos al carajo.
  var : ∀ ( v : Fin n) → Term  --(lookup v Γ)
  unit : Term
  _:∙:_ : Term {n}  → Term {n} → Term
  lam :  Term {suc n}  → Term 
  _,_ : Term {n}  → Term {n}  → Term   -- ( τ₁ X τ₂ )
  fst : Term {n}  → Term 
  snd :  Term {n} → Term 

 -- _⊢_ : { n : ℕ} → ( : Ctx n) → Type → Set
 --  ⊢ T = Term  T
-- https://github.com/jmchapman/Relative-Monads/blob/master/WellScopedTerms.agda

 -- Renaming
 Ren : ℕ → ℕ → Set
 Ren m n = Fin m → Fin n

 wk : ∀ {m n} → Ren m n → Ren (suc m) (suc n)
 wk f zero = zero
 wk f (suc n₁) = suc (f n₁)

 ren : ∀{m n } → Ren m n → Term {m}  → Term {n}
 ren f (var v) = var (f v)
 ren f unit = unit
 ren f (t :∙: t₁) = (ren f t) :∙: (ren f t₁)
 ren f (lam t) = lam (ren (wk f) t)
 ren f (t , t₁) = ( ren f t ,  ren f t₁ )
 ren f (fst t) = fst (ren f t)
 ren f (snd t) = snd (ren f t)

 -- Substitution
 Sub : ℕ → ℕ → Set
 Sub m n = Fin m → Term {n}

 liftT : ∀ {m n} → Sub m n → Sub (suc m) (suc n)
 liftT f zero = var zero
 liftT f (suc m₁) = ren suc (f m₁)

 sub : ∀{ m n } → Sub m n → Term {m} → Term {n}
 sub s (var v) = s v
 sub s unit = unit
 sub s (t :∙: t₁) = (sub s t) :∙: (sub s t₁)
 sub s (lam t) = lam (sub (liftT s) t)
 sub s (t , t₁) = sub s t , sub s t₁
 sub s (fst t) = fst (sub s t)
 sub s (snd t) = snd (sub s t)

 uploadFin : ∀{m n} → m Data.Nat.≤ n → Fin m → Fin n
 uploadFin (s≤s rel) zero = zero
 uploadFin (s≤s rel) (suc m₁) = suc (uploadFin rel m₁)

 subgen : ∀ {m n} → Fin m → Term {n} → Sub m n
 subgen x n y with x Data.Fin.Properties.≟ y
 subgen x n y | .Relation.Nullary.Dec.yes p = ?
 subgen x n y | .Relation.Nullary.Dec.no ¬p = ?
 --subgen rel (suc x) n₂ (suc y) with x Data.Fin.Properties.≟ y
 -- subgen rel (suc x) n₂ (suc y) | Relation.Nullary.Dec.yes p = ?
 -- subgen rel (suc x) n₂ (suc y) | Relation.Nullary.Dec.no ¬p = ?

 substs : ∀ {m n : ℕ} → Term {m} →  Fin m → Term {n} → Term {n}
 substs m x n = sub (λ y → {!!}) m
--  eval : ∀{ M n }{ Γ : Ctx n} → Γ ⊢ M → Fin n →  {!!}
--  eval t = {!!}
--  -- subst' : ∀{ M n N }{Γ : Ctx n} → Γ ⊢ M → Fin n → Γ ⊢ N → {!!}
--  -- subst' m x n = {!!}
--  -- --Play Ground
--  Closed : Type → Set
--  Closed t = [] ⊢ t 
 
--  Nat : Type
--  Nat = (₁ ⇒ ₁) ⇒ ₁ ⇒ ₁

--  zeroC : Closed Nat
--  zeroC = lam (₁ ⇒ ₁) (lam ₁ ((var (suc zero)) :∙: (var zero)))
 
--  sucC : Closed (Nat ⇒ Nat)
--  sucC = 
--    lam ((₁ ⇒ ₁) ⇒ ₁ ⇒ ₁)
--         (lam (₁ ⇒ ₁)
--         (lam ₁
--         ((var (suc zero)) 
--               :∙: (((var (suc (suc zero))) 
--               :∙: (var (suc zero))) 
--               :∙: (var zero)))))
-- -- parece que va por este lado: https://github.com/jmchapman/Relative-Monads/blob/master/WellScopedTerms.agda 
-- -- normalize :
-- -- \Forall... -> Γ ⊢ t1 =βη t2 : Dec ??
