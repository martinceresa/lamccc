module Typing where

--open import Bound Type
open import Data.Nat
open import Data.Vec
open import Data.Fin

data Type : Set where
    ⊤ : Type
    _X_ : Type → Type → Type
    _⇒_ : Type → Type → Type

infixr 20 _⇒_

data Expr (n : ℕ) : Set where
    ⋆ : Expr n
    var : (k : Fin n) → Expr n
    lam : (τ : Type) → Expr (suc n) → Expr n
    _∙_ : Expr n → Expr n → Expr n
    _,_ : Expr n → Expr n → Expr n
    fst : Expr n → Expr n
    snd : Expr n → Expr n

infixl 20 _∙_

Ctxt : ℕ → Set
Ctxt = Vec Type

data _⊢_∶_ : ∀{n} → Ctxt n → Expr n → Type → Set where
    tT : ∀{n} → {Γ : Ctxt n} → Γ ⊢ ⋆ ∶ ⊤
    tVar : ∀{n Γ}{x : Fin n} → Γ ⊢ var x ∶ lookup x Γ
    tLam : ∀{n}{Γ : Ctxt n}{τ E τ'}
         → (τ ∷ Γ) ⊢ E ∶ τ'
         → Γ ⊢ (lam τ E)∶ (τ ⇒ τ')
    _∙_ : ∀ {n} {Γ : Ctxt n} {E τ τ'} {F}
        → Γ ⊢ E ∶ (τ ⇒ τ')
        → Γ ⊢ F ∶ τ
        → Γ ⊢ (E ∙ F) ∶ τ'
    tpair : ∀{n E₁ E₂ τ₁ τ₂}{Γ : Ctxt n}
          → Γ ⊢ E₁ ∶ τ₁
          → Γ ⊢ E₂ ∶ τ₂
          → Γ ⊢ (E₁ , E₂) ∶ (τ₁ X τ₂)
    tfst : ∀{n E τ σ}{Γ : Ctxt n}
         → Γ ⊢ E ∶ (τ X σ) → Γ ⊢ (fst E) ∶ τ
    tsnd : ∀{n E τ σ}{Γ : Ctxt n}
         → Γ ⊢ E ∶ (τ X σ) → Γ ⊢ (snd E) ∶ σ
        -- pair constr

shiftone : ∀{n} → ( m : ℕ) → Expr n → Expr (n Data.Nat.+ m)
shiftone d (snd t) = {!!}
