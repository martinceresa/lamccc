open import Categories
open import Categories.CartesianClosed

open import Categories.Products
open import Categories.Products.Properties
open import Categories.Terminal


module Categories.CartesianClosed.Properties
       {a}{b} -- niveles
       {C : Cat {a}{b}} -- categoria
       (hasProducts : Products C)
       (T : Cat.Obj C)
       (hasTerminal : Terminal C T)
       (ccc : CCC hasProducts T hasTerminal)where

open import Library hiding (_×_;curry;uncurry;_+_)

open Cat C
open Products hasProducts
open CCC ccc

nat-uncurry : ∀ {X X' Y Z Z'}
            → {f : Hom X' X}
            → {h : Hom Z Z'}
            → {x : Hom X (Y ⇒ Z)}
            → h ∙ uncurry x ∙ ( pair f iden) ≅ uncurry ( map⇒ h ∙ x ∙ f)
nat-uncurry {f = f} {h} {x} = sym (proof
                                  uncurry (map⇒ h ∙ x ∙ f)
                                  ≅⟨ cong (λ q → uncurry (map⇒ h ∙ q ∙ f )) (sym lawcurry2) ⟩
                                  uncurry (map⇒ h ∙ curry (uncurry x) ∙ f)
                                  ≅⟨ cong uncurry nat-curry ⟩
                                  uncurry (curry (h ∙ uncurry x ∙ pair f iden))
                                  ≅⟨ lawcurry1 ⟩
                                  h ∙ uncurry x ∙ pair f iden
                                  ∎)

mapid : ∀{X Z } →  map⇒ iden ≅ iden {X ⇒ Z}
mapid = proof
        map⇒ iden
        ≅⟨ cong curry idl  ⟩
        curry apply
        ≅⟨ lawcurry2 ⟩
        iden
        ∎
nat-uncurry₁ : ∀ {X X' Y Z}
            → {f : Hom X' X}
            → {x : Hom X (Y ⇒ Z)}
            → uncurry x ∙ ( pair f iden) ≅ uncurry ( x ∙ f)
nat-uncurry₁ {f = f} {x} = trans (sym idl) (trans nat-uncurry
                (cong uncurry (proof
                              map⇒ iden ∙ x ∙ f
                              ≅⟨ cong (λ q → q ∙ x ∙ f ) mapid ⟩
                              iden ∙ x ∙ f
                              ≅⟨ idl ⟩
                              x ∙ f
                              ∎)))
nat-curry₁ : ∀ {X X' Y Z}
           → {f : Hom X' X}{x : Hom (X × Y) Z}
           →  curry (uncurry iden) ∙ curry x ∙ f ≅ curry ( x ∙ pair f iden)
nat-curry₁ { f = f} {x} = proof
             curry (uncurry iden) ∙ curry x ∙ f
             ≅⟨ congl (cong curry (sym idl)) ⟩
             curry (iden ∙ uncurry iden) ∙ curry x ∙ f
             ≅⟨ nat-curry ⟩
             curry (iden ∙ x ∙ pair f iden)
             ≅⟨ cong curry idl ⟩
             curry (x ∙ pair f iden)
             ∎

nat-curry-iden : ∀ {X X' Y Z}
           → {f : Hom X' X}{x : Hom (X × Y) Z}
           → curry x ∙ f ≅ curry ( x ∙ pair f iden)
nat-curry-iden {f = f} {x} =
    proof
    curry x ∙ f
    ≅⟨ sym idl ⟩
    iden ∙ curry x ∙ f
    ≅⟨ congl (sym lawcurry2) ⟩
    curry (uncurry iden) ∙ curry x ∙ f
    ≅⟨ nat-curry₁ ⟩
    curry (x ∙ pair f iden)
    ∎

sameDefs : ∀ {X Y Z}
        → {f : Hom (X × Y) Z}
        → apply ∙ (pair (curry f) iden) ≅ f
sameDefs {f = f} = trans
                      nat-uncurry₁
                      (trans
                      (cong uncurry idl) lawcurry1)
-- curr-app' : ∀ {A B C}
--          → {f : Hom (A × B) (B ⇒ C)}
--          → curry ( apply ∙ ⟨ f , π₂ ⟩ ) ≅ f
-- curr-app' {f = f} = proof
--             curry (apply ∙ ⟨ f , π₂ ⟩)
--             ≅⟨ {!!} ⟩
--             curry (apply ∙ ⟨ f , iden⟩ ∙ pair iden π₂ )
--             ≅⟨ {!!} ⟩
--             f
--             ∎

curr-app : ∀ {A B C}
         → {f : Hom A (B ⇒ C)}
         → curry (apply ∙ ⟨ f ∙ π₁ , π₂ ⟩) ≅ f
curr-app {f = f} = proof
                   curry (apply ∙ ⟨ f ∙ π₁ , π₂ ⟩)
                   ≅⟨ cong curry (congr (sym ( trans (fusion-pair hasProducts)
                     (law3 law1 (trans law2 idl))))) ⟩
                   curry (apply ∙ pair f iden ∙ ⟨ π₁ , π₂ ⟩)
                   ≅⟨ cong curry (congr (congr (sym (law3 idr idr)))) ⟩
                   curry (apply ∙ pair f iden ∙ iden )
                   ≅⟨ cong curry (congr idr) ⟩
                   curry (apply ∙ pair f iden)
                   ≅⟨ sym nat-curry₁ ⟩
                   curry (uncurry iden) ∙ curry apply ∙ f
                   ≅⟨ congl lawcurry2 ⟩
                   iden ∙ curry apply ∙ f
                   ≅⟨ idl ⟩
                   curry apply ∙ f
                   ≅⟨ congl lawcurry2 ⟩
                   iden ∙ f
                   ≅⟨ idl ⟩
                   f
                   ∎
apply-π₁ : ∀ {A B C}
         → {f : Hom A B }
         → {g : Hom A C}
         → apply ∙ ⟨ curry ( g ∙ π₁ ) , f ⟩ ≅ g
apply-π₁ {f = f} {g} = proof
                       apply ∙ ⟨ curry (g ∙ π₁) , f ⟩
                       ≅⟨ congr (sym (proof
                                     pair (curry (g ∙ π₁)) iden ∙ ⟨ iden , f ⟩
                                     ≅⟨ fusion-pair hasProducts ⟩
                                     ⟨ curry (g ∙ π₁) ∙ iden , iden ∙ f ⟩
                                     ≅⟨ law3 (trans law1 idr) (trans law2 idl) ⟩
                                     ⟨ curry (g ∙ π₁) , f ⟩
                                     ∎)) ⟩
                        apply ∙  pair (curry ( g ∙ π₁)) iden ∙ ⟨ iden , f ⟩
                       ≅⟨ sym ass ⟩
                       ( apply ∙  pair (curry ( g ∙ π₁)) iden ) ∙ ⟨ iden , f ⟩
                       ≅⟨ congl sameDefs ⟩
                       (g ∙ π₁) ∙ ⟨ iden , f ⟩
                       ≅⟨ ass ⟩
                       g ∙ π₁ ∙ ⟨ iden , f ⟩
                       ≅⟨ congr law1 ⟩
                       g ∙ iden
                       ≅⟨ idr ⟩
                       g
                       ∎

apply-π₂' : ∀ {A B}
         → {f : Hom A B}
         → apply ∙ pair (curry π₂) iden ∙ ⟨ iden , f ⟩  ≅ f
apply-π₂' {f = f } = proof
                    apply ∙ pair (curry π₂) iden ∙ ⟨ iden , f ⟩
                    ≅⟨ sym ass ⟩
                     (apply ∙ pair (curry π₂) iden) ∙ ⟨ iden , f ⟩
                    ≅⟨ congl nat-uncurry₁ ⟩
                    uncurry (iden ∙ curry π₂) ∙ ⟨ iden , f ⟩
                    ≅⟨ congl (cong uncurry idl) ⟩
                     uncurry (curry π₂) ∙ ⟨ iden , f ⟩
                    ≅⟨ congl lawcurry1 ⟩
                    π₂ ∙ ⟨ iden , f ⟩ --π₂ ∙ ⟨iden , f ⟩
                    ≅⟨ law2 ⟩
                    f
                    ∎
apply-π₂ : ∀ {A B}
         → {f : Hom A B}
         → apply ∙ ⟨ curry π₂ , f ⟩ ≅ f
apply-π₂ {f = f} = proof
                   apply ∙ ⟨ curry π₂ , f ⟩
                   ≅⟨ cong (λ q → apply ∙ q) (sym (trans (fusion-pair hasProducts)
                     (trans (cong (λ q → ⟨ q  , iden ∙ f ⟩) idr)
                     (trans (cong (λ q → ⟨_,_⟩ (curry π₂) q) idl) refl)))) ⟩
                   apply ∙ pair (curry π₂) iden ∙ ⟨ iden , f ⟩
                   ≅⟨ apply-π₂' ⟩
                   f
                   ∎

another : ∀ {A B C}
        → {g : Hom A (B ⇒ C)}
        → apply ∙ (pair g iden) ≅ uncurry g
another {g = g } = trans nat-uncurry₁ (cong uncurry idl)

sameDef : ∀ { X Y Z }
        → {f : Hom (X × Y) Z}
        → curry ( iden ∙ uncurry iden ∙ (pair (curry f) iden)) ≅ curry f
sameDef {f = f} = proof
                  curry (iden ∙ uncurry iden ∙ pair (curry f) iden)
                  ≅⟨ sym nat-curry ⟩
                  curry (iden ∙ uncurry iden) ∙ curry (uncurry iden) ∙ curry f
                  ≅⟨ cong (λ q → q ∙  curry (uncurry iden) ∙ curry f ) (cong (λ q → curry q) idl) ⟩
                  curry (uncurry iden) ∙ curry (uncurry iden) ∙ curry f
                  ≅⟨ cong (λ q → q ∙ curry (uncurry iden) ∙ curry f) lawcurry2 ⟩
                  iden ∙ curry (uncurry iden) ∙ curry f
                  ≅⟨ idl ⟩
                  curry (uncurry iden) ∙ curry f
                  ≅⟨ cong (λ q → q ∙ curry f) lawcurry2 ⟩
                  iden ∙ curry f
                  ≅⟨ idl ⟩
                  curry f
                  ∎
