open import lamInd
--open import Stp
module Playground where

idTerm : ∀{τ} → Tm ε (τ ⇒ τ)
idTerm = Λ (var vz)

NatTy : (τ : Ty) → Ty
NatTy τ = (τ ⇒ τ) ⇒ τ ⇒ τ

NatU : Ty
NatU =  NatTy 𝟙 -- (𝟙 ⇒  𝟙) ⇒ 𝟙 ⇒ 𝟙

zeroC : {τ : Ty } → Tm ε (NatTy τ)
zeroC = Λ (Λ (var (vz)))

sucNat : {τ : Ty} → Tm ε (NatTy τ ⇒ NatTy τ)
sucNat = Λ -- n
         (Λ -- s
         (Λ -- z
         (let n = var (vs (vs vz))
              s = var (vs vz)
              z = var vz
          in s  ∙ (n ∙ s ∙ z))))

oneC : {τ : Ty} → Tm ε (NatTy τ)
oneC = Λ (Λ ((var (vs vz)) ∙ (var vz)))

oneEq : ∀{τ} → (sucNat ∙ zeroC) βη-≡ oneC {τ}
oneEq {τ} = btrans (beta {ε} {NatTy τ} {NatTy τ})
                   (congΛ {ε} {τ ⇒ τ} {τ ⇒ τ} 
                   (congΛ {ε , (τ ⇒ τ)} {τ} {τ}
                   (congApp {(ε , (τ ⇒ τ)) , τ} {τ} {τ}
                            {var (vs vz)} {var (vs vz)}
                            {Λ (Λ (var vz)) ∙ var (vs vz) ∙ var vz} {var vz}
                            brefl
                            (btrans (congApp beta brefl) beta))))

segundo : ∀ {σ σ₁}  → Tm ε  (σ₁ ⇒ σ ⇒ σ)
segundo = Λ (Λ (var vz))

appseg : Tm ε 𝟙
appseg = segundo ∙ unit ∙ unit
