open import Categories
open import Categories.Products
open import Categories.Terminal
open import Library hiding ( _×_ ; curry)
open import Categories.CartesianClosed

open import lamInd renaming (_∙_ to _∘_ ; pair to ppx)
open import Playground

module Stp {a}{b}{C : Cat {a}{b}}
           (hasProducts : Products C)
           (T : Cat.Obj C)
           (hasTerminal : Terminal C T)
           (ccc : CCC hasProducts T hasTerminal)
       where

open
    import
    Categories.Products.Properties
    {a}{b}{C} hasProducts
open
    import
    Categories.CartesianClosed.Properties
    {a}{b}{C} hasProducts T hasTerminal ccc
open Cat C
open Products hasProducts
open Terminal hasTerminal
open CCC ccc renaming ( _⇒_ to _^_ )

⟦_⟧ₜ : Ty → Obj
⟦ 𝟙 ⟧ₜ = T
⟦ t Ty.⇒ t₁ ⟧ₜ = ⟦ t ⟧ₜ ^  ⟦ t₁ ⟧ₜ
⟦ A X B ⟧ₜ = ⟦ A ⟧ₜ × ⟦ B ⟧ₜ

⟦_⟧ : Con → Obj
⟦ ε ⟧ = T
⟦ c , t ⟧ = ⟦ c ⟧ ×  ⟦ t ⟧ₜ

⟦_⟧ₗ : ∀ {C T} → Tm C T → Hom ⟦ C ⟧ ⟦ T ⟧ₜ
⟦ unit ⟧ₗ = t
⟦_⟧ₗ {ε} (var ())
⟦_⟧ₗ {C₁ , T} (var vz) = π₂
⟦_⟧ₗ {C₁ , τ} (var (vs x₁)) =  ⟦ var x₁ ⟧ₗ ∙ π₁
⟦ Λ t ⟧ₗ = curry ⟦ t ⟧ₗ
⟦ t Tm.∙ t₁ ⟧ₗ = apply ∙ ⟨ ⟦ t ⟧ₗ ,  ⟦ t₁ ⟧ₗ ⟩
⟦ t , t₁ ⟧ₗ = ⟨ ⟦ t ⟧ₗ , ⟦ t₁ ⟧ₗ ⟩
⟦ fst t ⟧ₗ =  π₁ ∙ ⟦ t ⟧ₗ
⟦ snd t ⟧ₗ = π₂ ∙ ⟦ t ⟧ₗ

zeroCCat : Hom ⟦ ε ⟧ ⟦ NatTy 𝟙 ⟧ₜ
zeroCCat =  ⟦ zeroC {𝟙} ⟧ₗ

unoCCat : Hom ⟦ ε ⟧ ⟦ NatU ⟧ₜ
unoCCat = ⟦ oneC {𝟙} ⟧ₗ

propProd : ∀{A B C }
         → Hom ((A × B) × C) (A × B × C)
propProd = ⟨ π₁ ∙ π₁  , pair π₂ iden ⟩
recode : ∀ {σ }
       → (Γ : Con)
       → (x : Var Γ σ)
       → Hom ⟦ Γ ⟧ (⟦ split₁ {σ} Γ x ⟧ × ⟦ split₂ Γ x ⟧)
recode ε ()
recode (Γ , σ) vz = ⟨ π₁ , t ⟩
recode (Γ , τ) (vs x₁) with recode Γ x₁
... | rec = propProd ∙ pair rec iden

recode' : ∀ {σ}
        → (Γ : Con)
        → (x : Var Γ σ)
        → Hom ⟦ Γ ⟧ ((⟦ split₁ {σ} Γ x ⟧ × ⟦ σ ⟧ₜ ) × ⟦ split₂ Γ x ⟧)
recode' ε ()
recode' (Γ , σ) vz = ⟨ iden , t ⟩
recode' (Γ , τ) (vs x₁) with recode' Γ x₁
... | rec = propProd ∙ pair rec iden 

unioncon : ∀ {Γ₁ Γ₂}
         → Hom (⟦ Γ₁ ⟧ × ⟦ Γ₂ ⟧) (⟦ concatCon Γ₁ Γ₂ ⟧)
unioncon {Γ₂ = ε} = π₁
unioncon {Γ} {Γ₂ = Γ₂ , x} with unioncon {Γ} {Γ₂}
... | rec = ⟨ rec ∙ pair iden π₁ , π₂ ∙ π₂ ⟩

code : ∀{Γ σ}
     → (x : Var Γ σ)
     → Tm (Γ - x) σ
     → Hom ⟦ Γ - x ⟧ ⟦ Γ ⟧
code {ε} () u
code {Γ , σ} vz u = ⟨ iden , ⟦ u ⟧ₗ ⟩
code {_ , τ} (vs vz) u = pair (code vz {!u!}) iden
code {_ , τ₁} (vs (vs x₁)) u = {!!}
--code (vs x) u = pair {!!} iden
encodeVar : ∀ { Γ σ τ }
          → Var Γ σ
          → (x : Var Γ τ)
          → Tm (Γ - x) τ → Hom ⟦ Γ - x ⟧ ⟦ σ ⟧ₜ
encodeVar v₁ v₂ t with eq v₂ v₁
encodeVar v₁ .v₁ t | same = ⟦ t ⟧ₗ
encodeVar .(wkv x y) x t | diff .x y = ⟦ var y ⟧ₗ

lemmaVar : ∀ { Γ σ τ }
          → (v : Var Γ σ )
          → (x : Var Γ τ)
          → (u : Tm (Γ - x) τ)
          → encodeVar v x u ≅ ⟦ substVar v x u ⟧ₗ
lemmaVar v x u with eq x v
lemmaVar v .v u | same = refl
lemmaVar .(wkv x y) x u | diff .x y = refl

encode : ∀{Γ σ τ}
          → (t : Tm Γ τ)
          → (x : Var Γ σ)
          → (u : Tm (Γ - x) σ)
          → Hom ⟦ Γ  -  x ⟧ ⟦ τ ⟧ₜ
encode unit x u = t
encode (var x) x₁ u = encodeVar x x₁ u
encode (Λ t) x u = curry (encode t (vs x) (wkTm vz u))
encode (t Tm.∙ t₁) x u = apply ∙  ⟨ encode t x u , encode t₁ x u ⟩
encode (t , t₁) x u = ⟨ encode t x u , encode t₁ x u ⟩
encode (Tm.fst t) x u =  π₁ ∙ encode t x u
encode (Tm.snd t) x u = π₂ ∙ encode t x u
    --  ⟦ t ⟧ₗ ∙ (code x u) ≅ ⟦ lamInd.subst t x u ⟧ₗ

termsubst : ∀{Γ σ τ}
          → (t : Tm Γ τ)
          → (x : Var Γ σ)
          → (u : Tm (Γ - x) σ)
          → encode t x u ≅ ⟦ lamInd.subst t x u ⟧ₗ
termsubst unit x u = refl
termsubst (var x) x₁ u = lemmaVar x x₁ u
termsubst (Λ t) x u = cong curry ( termsubst t (vs x) (wkTm vz u) )
termsubst (t Tm.∙ t₁) x u = congr (law3 (trans law1 (termsubst t x u)) (trans law2 (termsubst t₁ x u)))
termsubst (t , t₁) x u = law3 (trans law1 (termsubst t x u)) (trans law2 (termsubst t₁ x u))
termsubst (Tm.fst t) x u = congr (termsubst t x u)
termsubst (Tm.snd t) x u = congr (termsubst t x u)

subst-∙1 : ∀ {Γ σ τ}
        → (t : Tm (Γ , σ ) τ)
        → (u : Tm Γ σ)
        → ⟦ t ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩   ≅ encode t vz u
subst-∙1 unit u = sym law
subst-∙1 (var vz) u = law2
subst-∙1 (var (vs x)) u = trans ass (trans (congr law1) idr)
subst-∙1 (Λ t) u =
    proof
    ⟦ Λ t ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅⟨ nat-curry-iden ⟩
    curry (⟦ t ⟧ₗ ∙ pair ⟨ iden , ⟦ u ⟧ₗ ⟩ iden)
    ≅⟨ cong curry {!!} ⟩
    encode (Λ t) vz u
    ∎
subst-∙1 (t Tm.∙ t₁) u = {!!}
subst-∙1 (t , t₁) u = {!!}
subst-∙1 (Tm.fst t) u = {!!}
subst-∙1 (Tm.snd t) u = {!!}

subst-∙ : ∀ {Γ σ τ}
        → (t : Tm (Γ , σ ) τ)
        → (u : Tm Γ σ)
        → ⟦ t ⟧ₗ ∙  ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅ ⟦ lamInd.subst t vz u ⟧ₗ
-- tengo que generalizar esto!!
subst-∙ unit u = sym law
subst-∙ (var vz) u = law2
subst-∙ (var (vs x)) u = trans ass (trans (congr law1) idr)
subst-∙ (Λ t) u =
    proof
    ⟦ Λ t ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅⟨ nat-curry-iden ⟩
    curry (⟦ t ⟧ₗ ∙ pair ⟨ iden , ⟦ u ⟧ₗ ⟩ iden)
    ≅⟨ {!!} ⟩ --  cong curry (termsubst t (vs vz) (wkTm vz u)) ⟩
    ⟦ lamInd.subst (Λ t) vz u ⟧ₗ
    ∎

subst-∙ (t Tm.∙ t₁) u =
    proof
    ⟦ t Tm.∙ t₁ ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅⟨ ass ⟩
    apply ∙ ⟨ ⟦ t ⟧ₗ , ⟦ t₁ ⟧ₗ ⟩ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅⟨ congr (fusion) ⟩
    apply ∙
      ⟨ ⟦ t ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩ , ⟦ t₁ ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩ ⟩
    ≅⟨ congr (law3 (trans law1 (subst-∙ t u)) (trans law2 (subst-∙ t₁ u))) ⟩
    ⟦ lamInd.subst (t Tm.∙ t₁) vz u ⟧ₗ
    ∎
subst-∙ (t , t₁) u =
    proof
    ⟨ ⟦ t ⟧ₗ , ⟦ t₁ ⟧ₗ ⟩ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅⟨ fusion ⟩
    ⟨ ⟦ t ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩ , ⟦ t₁ ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩ ⟩
    ≅⟨ law3 (trans law1 (subst-∙ t u)) (trans law2 (subst-∙ t₁ u)) ⟩
    ⟦ lamInd.subst (t , t₁) vz u ⟧ₗ
    ∎
subst-∙ (Tm.fst t) u = trans ass (congr (subst-∙ t u))
subst-∙ (Tm.snd t) u = trans ass (congr (subst-∙ t u))

denSem' : ∀ {A C} → {a b : Tm C A} → a βη-≡ b → ⟦ a ⟧ₗ ≅ ⟦ b ⟧ₗ
denSem' brefl = refl
denSem' (bsym eq) = sym (denSem' eq)
denSem' (btrans eq eq₁) = trans (denSem' eq) (denSem' eq₁)
denSem' (congΛ eq) = cong curry (denSem' eq)
denSem' (congApp eq eq₁) = cong ( λ q → apply ∙ q)
                         (cong₂ ⟨_,_⟩ (denSem' eq) (denSem' eq₁))
denSem' (beta {t = t} {u}) =
    proof
    ⟦ Λ t Tm.∙ u ⟧ₗ
    ≅⟨ congr utili ⟩
    apply ∙ pair ⟦ Λ t ⟧ₗ iden ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅⟨ sym ass ⟩
    (apply ∙ pair ⟦ Λ t ⟧ₗ iden) ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅⟨ congl sameDefs ⟩
    ⟦ t ⟧ₗ ∙ ⟨ iden , ⟦ u ⟧ₗ ⟩
    ≅⟨ subst-∙ t u ⟩
    ⟦ lamInd.subst t vz u ⟧ₗ
    ∎
denSem' {b = var x} eta = curr-app
denSem' {b = Λ b₁} eta =
  cong curry
    (proof
    ⟦ wkTm vz (Λ b₁) Tm.∙ var vz ⟧ₗ
    ≅⟨ congr utili ⟩
    apply ∙ pair ⟦ wkTm vz (Λ b₁) ⟧ₗ iden ∙ ⟨ iden , ⟦ var vz ⟧ₗ ⟩
    ≅⟨ sym ass ⟩
    (apply ∙ pair ⟦ wkTm vz (Λ b₁) ⟧ₗ iden) ∙ ⟨ iden , ⟦ var vz ⟧ₗ ⟩
    ≅⟨ congl sameDefs ⟩
    ⟦ wkTm (vs vz) b₁ ⟧ₗ ∙ ⟨ iden , ⟦ var vz ⟧ₗ ⟩
    ≅⟨ {!!} ⟩
    ⟦ b₁ ⟧ₗ
    ∎)
denSem' {b = b₁ Tm.∙ b₂} eta = {!!}
denSem' {b = Tm.fst b₁} eta = {!!}
denSem' {b = Tm.snd b₁} eta = {!!}
denSem' first = law1
denSem' second = law2
denSem' (ppx {c = c} ) = stpProp 
    -- with ⟦ c ⟧ₗ
-- ... | res = sym {!!}

denSem : ∀ { A B } → (a b : Tm ε (A ⇒ B)) → a βη-≡ b → ⟦ a ⟧ₗ ≅ ⟦ b ⟧ₗ
denSem b .b brefl = refl
denSem a b (bsym eq) = sym (denSem b a eq)
denSem p b (btrans {q = q} eq eq₁) = trans (denSem p q eq) (denSem q b eq₁)
denSem _ _ (congΛ {p = p} {q = q} eq) = {!!}
denSem _ _ (congApp eq eq₁) = {!!}
denSem _ _ beta = {!!}
denSem .(Λ (wkTm vz b Tm.∙ var vz)) b eta = {!!}
denSem _ b first = {!!}
denSem _ b second = {!!}
