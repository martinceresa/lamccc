module lamInd where
open import Library hiding (fst ; snd ; subst)

infixr 90 _⇒_
data Ty : Set where
    𝟙 : Ty
    _⇒_ : Ty → Ty → Ty
    _X_ : Ty → Ty → Ty

data Con : Set where
    ε : Con
    _,_ : Con → Ty → Con

data Var : Con → Ty → Set where
    vz : ∀ {Γ σ} → Var (Γ , σ) σ
    vs : ∀ {τ Γ σ} → Var Γ σ → Var (Γ , τ) σ

infixl 80 _∙_
data Tm : Con → Ty → Set where
    unit : ∀ {Γ} → Tm Γ 𝟙
    var : ∀ {Γ σ} → Var Γ σ → Tm Γ σ
    Λ   : ∀ {Γ σ τ} → Tm ( Γ , σ ) τ → Tm Γ (σ ⇒ τ)
    _∙_ : ∀ {Γ σ τ} → Tm Γ (σ ⇒ τ) → Tm Γ σ → Tm Γ τ
    _,_ : ∀ {Γ σ τ} → Tm Γ σ → Tm Γ τ → Tm Γ ( σ X τ )
    fst : ∀ {Γ σ τ} → Tm Γ (σ X τ) → Tm Γ σ
    snd : ∀ {Γ σ τ} → Tm Γ (σ X τ) → Tm Γ τ

_-_ : {σ : Ty} → (Γ : Con) → Var Γ σ → Con
ε - ()
(Γ , σ) - vz = Γ
(Γ , τ) - vs v =   ( Γ - v ) , τ

split : {σ : Ty} → (Γ : Con) → Var Γ σ → Con × Con
split ε ()
split (g , σ) vz = g , ε
split (g , τ) (vs s) with split g s
split (g , τ) (vs s) | proj₁ , proj₂ = proj₁ , (proj₂ , τ)

split₁ : ∀{σ} → (Γ : Con) → Var Γ σ → Con
split₁ ε ()
split₁ (Γ , σ) vz = Γ
split₁ (Γ , τ) (vs x₁) = split₁ Γ x₁

split₂ : ∀{σ} → (Γ : Con) → Var Γ σ → Con
split₂ ε ()
split₂ (Γ , σ) vz = ε
split₂ (Γ , τ) (vs x₁) =  (split₂ Γ x₁) , τ

concatCon : Con → Con → Con
concatCon Γ ε = Γ
concatCon Γ (Σ , x) =  concatCon Γ Σ , x

wkv : ∀ {Γ σ τ} → (x : Var Γ σ) → Var (Γ - x) τ → Var Γ τ
wkv vz v = vs v
wkv (vs x) vz = vz
wkv (vs x) (vs v) = vs (wkv x v)

data EqV : ∀{Γ σ τ} → Var Γ σ → Var Γ τ → Set where
    same : ∀ {Γ σ} {x : Var Γ σ} → EqV x x
    diff : ∀ {Γ σ τ} → (x : Var Γ σ) → (y :  Var (Γ - x) τ)
           → EqV x (wkv x y)

eq : ∀{Γ σ τ} → (x : Var Γ σ)
              → (y : Var Γ τ)
              → EqV x y
eq vz vz = same
eq vz (vs y) = diff vz y
eq (vs x) vz = diff (vs x) vz
eq (vs x) (vs y) with eq x y
eq (vs y) (vs .y) | same = same
eq (vs x) (vs .(wkv x y)) | diff .x y = diff (vs x) (vs y)

wkTm : ∀{ Γ σ τ } → (x : Var Γ σ) → Tm (Γ - x) τ → Tm Γ τ
wkTm x unit = unit
wkTm x (var x₁) = var (wkv x x₁)
wkTm x (Λ t) = Λ (wkTm (vs x) t)
wkTm x (t ∙ t₁) = (wkTm x t) ∙ (wkTm x t₁)
wkTm x (p , p₁) = (wkTm x p) , (wkTm x p₁)
wkTm x (fst p) = fst (wkTm x p)
wkTm x (snd p) = snd (wkTm x p)

substVar : ∀ {Γ σ τ} → Var Γ σ
                     → (x : Var Γ τ)
                     → Tm (Γ - x) τ → Tm (Γ - x) σ
substVar v₁ v₂ t with eq v₂ v₁
substVar v₁       .v₁ t | same      = t
substVar .(wkv x y) x t | diff .x y = var y

subst : ∀{ Γ σ τ } → Tm Γ τ       -- M
                   → (x : Var Γ σ)-- x
                   → Tm (Γ - x) σ -- N
                   → Tm (Γ - x) τ -- M [ N / x]
subst unit x n = unit
subst (var x) x₁ n = substVar x x₁ n
subst (Λ m) x n = Λ (subst m (vs x) (wkTm vz n))
subst (m ∙ m₁) x n = (subst m x n) ∙ (subst m₁ x n)
subst (t , t₁) x n = (subst t x n) , (subst t₁ x n)
subst (fst t) x n = fst (subst t x n)
subst (snd t) x n = snd (subst t x n)

data _βη-≡_ {Γ : Con} : ∀{σ} → Tm Γ σ → Tm Γ σ → Set where
    brefl : ∀{ σ } → {t : Tm Γ σ} → t βη-≡ t
    bsym :  ∀{σ} → {p q : Tm Γ σ} → p βη-≡ q → q βη-≡ p
    btrans : ∀{σ} → {p q r : Tm Γ σ}
                    → p βη-≡ q
                    → q βη-≡ r
                    → p βη-≡ r
    congΛ : ∀{σ τ} → {p q : Tm (Γ , τ) σ} →
                   p βη-≡ q → Λ p βη-≡ Λ q
    congApp : ∀{ σ τ}
              → {p q : Tm Γ (σ ⇒ τ) }
              → {u v : Tm Γ σ}
              → p βη-≡ q
              → u βη-≡ v
              → (p ∙ u) βη-≡ (q ∙ v)
    beta : ∀{ σ τ} →
         {t : Tm ( Γ , σ) τ } → {u : Tm Γ σ} →
         ((Λ t) ∙ u ) βη-≡ subst t vz u
    eta : ∀{ σ τ} → {t : Tm Γ (σ ⇒ τ)} →
        Λ ( wkTm vz t ∙ (var vz) ) βη-≡ t
    first : ∀ {σ τ} → {q : Tm Γ σ}
                    → {p : Tm Γ τ}
                    → fst (q , p) βη-≡ q
    second : ∀ {σ τ} → {q : Tm Γ σ}
                     → {p : Tm Γ τ}
                     → snd (q , p) βη-≡ p
    pair : ∀ {σ τ} → {c : Tm Γ (σ X τ)}
                   → ( (fst c) , (snd c) ) βη-≡ c

-- Normalization
-- data Ne : Con → Ty → Set
-- mutual
--   data Nf : Con → Ty → Set where
--      λn : ∀{Γ σ τ} → Nf ( Γ , σ ) τ → Nf Γ (σ ⇒ τ)
--      ne : ∀ {Γ} → Ne Γ 𝟙 → Nf Γ 𝟙
--      pn : ∀ {Γ σ τ} → Nf Γ σ → Nf Γ τ → Nf Γ (σ X τ)
-- --     fn : ∀ {Γ σ τ} → Nf Γ (σ X τ) → Nf Γ σ
-- --     sn : ∀ {Γ σ τ} → Nf Γ (σ X τ) → Nf Γ τ

--   -- data Sp : Con → Ty → Ty → Set where
--   --    ε : ∀{Γ σ} → Sp Γ σ σ
--   --    _,_ : ∀{Γ σ τ ρ} → Nf Γ τ → Sp Γ σ ρ → Sp Γ (τ ⇒ σ) ρ
--   data Ne : Con → Ty → Set where
-- --     _,_ : ∀{Γ σ τ} → Var Γ σ → Sp Γ σ τ → Ne Γ τ
--      ve : ∀{Γ σ} → Var Γ σ → Ne Γ σ
--      appe : ∀{Γ σ τ} → Ne Γ (σ ⇒ τ) → Nf Γ σ → Ne Γ τ
--      fe : ∀{Γ σ τ} → Ne Γ (σ X τ) → Ne Γ σ
--      se : ∀{Γ σ τ} → Ne Γ (σ X τ) → Ne Γ τ

-- mutual
--   ⌈_⌉ : ∀{Γ σ} → Nf Γ σ → Tm Γ σ
--   ⌈ λn t ⌉ = Λ ⌈ t ⌉
--   ⌈ ne x ⌉ = embNe x
--   ⌈ pn s s₁ ⌉ = ⌈ s ⌉ , ⌈ s₁ ⌉
--   -- ⌈ fn t ⌉ = fst ⌈ t ⌉
--   -- ⌈ sn t ⌉ = snd ⌈ t ⌉
--   embNe : ∀{Γ σ} → Ne Γ σ → Tm Γ σ
--   embNe (appe e x) = embNe e ∙ ⌈ x ⌉
--   embNe (fe e) = fst (embNe e)
--   embNe (se e) = snd (embNe e)
--   embNe (ve x) = var x

-- mutual
--     wkNe : ∀{Γ σ τ} → (x : Var Γ σ) → Ne (Γ - x) τ → Ne Γ τ
--     wkNe x (appe n x₁) = appe (wkNe x n) (wkNf x x₁)
--     wkNe x (fe n) = fe (wkNe x n)
--     wkNe x (se n) = se (wkNe x n)
--     wkNe x (ve x₁) = ve (wkv x x₁)
--     wkNf : ∀{Γ σ τ} → (x : Var Γ σ) → Nf (Γ - x) τ → Nf Γ τ
--     wkNf x (λn nf) = λn (wkNf (vs x) nf)
--     wkNf x (ne x₁) = ne (wkNe x x₁)
--     wkNf x (pn nf nf₁) = pn (wkNf x nf) (wkNf x nf₁)

-- mutual
--   nvar : ∀{Γ σ} → Var Γ σ → Nf Γ σ
--   nvar {σ = 𝟙} t = ne (ve t)
--   nvar {σ = σ ⇒ σ₁} t = λn {!!}
--   nvar {σ = σ X σ₁} t = {!!}

--   ne2nf : ∀{Γ σ} → Ne Γ σ → Nf Γ σ
--   ne2nf n = {!n!}
-- --     -- ne2nf {σ = σ ⇒ σ₁} (x , ns) = λn (ne2nf ((vs x) , addSp (wkSp vz ns) (nvar vz)))
-- --     -- ne2nf {σ = σ ⇒ σ₁} (fe t) = {!!} -- = λn {!!}
-- --     -- ne2nf {σ = σ ⇒ σ₁} (se t) = {!!}
-- --     -- ne2nf {σ = σ X σ₁} (x , x₁) = {!!}
-- --     -- ne2nf {σ = σ X σ₁} (fe t) = {!!}
-- --     -- ne2nf {σ = σ X σ₁} (se t) = {!!}
