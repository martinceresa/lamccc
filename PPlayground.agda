open import PHOAS
open SimpleTyped
module PPlayground where
idTerm : ∀ {τ} → Term (τ ⇒ τ)
idTerm = lam (λ x → var x)

idApp : ∀ {τ} → Term (τ ⇒ τ)
idApp {τ} = (idTerm {τ ⇒ τ}) :∙: (idTerm {τ})

Nat : Type → Type
Nat τ = ((τ ⇒ τ) ⇒ τ ⇒ τ)

zeroChurch : ∀ {τ} → Term (Nat τ)
zeroChurch = lam (λ f → lam (λ x → (var x)))

unocChurch : ∀{τ} → Term (Nat τ)
unocChurch = lam (λ f → lam (λ x →  var f :∙: var x))
sucChurch : ∀{τ} → Term ((Nat τ) ⇒ (Nat τ))
sucChurch =
     lam
       (λ n →
       lam
         (λ f →
           lam (λ x → (var f) :∙: (((var n) :∙: (var f)) :∙: (var x)))))

unocC : ∀{τ} → Term (Nat τ)
unocC = sucChurch :∙: zeroChurch

unoxdos : ∀{τ} → Term ( (Nat τ) X (Nat τ))
unoxdos = zeroChurch ⊗ (sucChurch :∙: zeroChurch)

unonuno : ∀{τ} → unocC βη-≡ unocChurch {τ}
unonuno = btrans beta {!!}
-- omega : ∀{τ} → Term {!!}
-- omega = (lam (λ x → (var x) :∙: (var x))) :∙: (lam (λ x → (var x) :∙: (var x)))
