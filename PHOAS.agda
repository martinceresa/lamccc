-- Implementamos PHOAS! NO GO!
-- + http://adam.chlipala.net/papers/PhoasICFP08/
-- + Boxes goes BANANAS :D!
module PHOAS where
-- Untyped Terms
 module Untyped where
   infixr 40 _⊗_
   infixr 90 _:∙:_
   data term (V : Set) : Set where
     var : V → term V
     u : term V
     _:∙:_ : term V → term V → term V
     lam : (V → term V) → term V
     _⊗_ : term V → term V → term V
     fst : term V → term V
     snd : term V → term V

   Term : Set₁
   Term = ∀ {V} → term V

   idTerm : Term
   idTerm = lam (λ x → var x)

   zeroChurch : Term
   zeroChurch = lam (λ f → lam (λ x → (var f) :∙: (var x)))

   sucChurch : Term
   sucChurch = lam (λ n → lam (λ f → lam (λ x → (var f) :∙: (((var n) :∙: (var f)) :∙: var x))))

   unoC : Term
   unoC = sucChurch :∙: zeroChurch

   dosC : Term
   dosC = sucChurch :∙: unoC

   omega : Term
   omega = (lam (λ x → (var x) :∙: (var x))) :∙: (lam (λ x → (var x) :∙: (var x)))

   she : Term
   she = lam (λ f → (lam (λ x → var f :∙:  var x :∙: var x)) :∙: (lam (λ x → var f :∙:  var x :∙: var x)))

 module SimpleTyped where
   infixr 30 _⇒_
   data Type : Set where
     -- | Unit type
     ₁ : Type
     -- | Function Type, A -> B
     _⇒_ : Type → Type → Type
     -- | Product Type, A x B
     _X_ : Type → Type → Type

--   infixr 40 _⊗_
--   infixr 90 _:∙:_
   data term (V : Type → Set) : Type → Set where
     var : {τ : Type} → V τ → term V τ
     u : term V ₁
     _:∙:_ : {τ₁ τ₂ : Type} → (term V (τ₁ ⇒ τ₂)) → term V τ₁ → term V τ₂
     lam : {τ₁ τ₂ : Type} → (V τ₁ → term V τ₂) → (term V (τ₁ ⇒ τ₂))
     _⊗_ : {τ₁ τ₂ : Type} → term V τ₁ → term V τ₂ → term V (τ₁ X τ₂)
     fst : {τ₁ τ₂ : Type} → term V (τ₁ X τ₂) → term V τ₁
     snd : {τ₁ τ₂ : Type} → term V (τ₁ X τ₂) → term V τ₂

   Term : Type → Set₁
   Term τ = ∀ {V} → term V τ

   subst : ∀{V τ} → term (term V) τ  → term V τ
   subst (var t) = t
   subst u = u
   subst (t :∙: t₁) = (subst t) :∙: (subst t₁)
   subst (lam x) = lam (λ x₁ → subst (x (var x₁)))
   subst (t ⊗ t₁) = subst t ⊗ subst t₁ 
   subst (fst t) = fst (subst t)
   subst (snd t) = snd (subst t)

   data _≡v_ : (v1 v2 : Type → Set) → Set where
    α-refl : {v : Type → Set} → v ≡v v

   data _βη-≡_  {V : Type → Set} : ∀{τ} → term V τ → term V τ → Set where
     brefl : ∀{σ } → {t : term V σ } → t βη-≡ t
     bsym : ∀{σ } → {p q : term V σ} → p βη-≡ q → q βη-≡ p
     btrans : ∀{σ} → {p q r : term V σ}
            → p βη-≡ q
            → q βη-≡ r
            → p βη-≡ r
     congLam : ∀{σ τ} → {p q : V σ → term V τ}
             → (∀{x} → p x βη-≡ q x) → lam p βη-≡ lam q
     congApp : ∀{σ τ}
             → { p q : term V (σ ⇒ τ) }
             → {u v : term V σ}
             → p βη-≡ q
             → u βη-≡ v
             → (p :∙: u) βη-≡ (q :∙: v)
     beta : ∀{σ τ} -- esto está medio al pedo?
          → {t : term V σ → term (term V) τ}
          → {u : term V σ} →
          (lam (λ x → subst (t (var x))) :∙: u) βη-≡ subst (t u)
          --((lam (λ x → subst (t (var x)))) :∙: u) βη-≡  subst (t u)
     eta : ∀{σ τ}
         → {t : term V (σ ⇒ τ)}
         → lam (λ x →  t :∙: var x) βη-≡ t
     first : ∀ {σ τ}
           → {q : term V σ}
           → {p : term V τ}
           → fst (q ⊗ p) βη-≡ q
     snd : ∀ {σ τ}
         → {q : term V σ}
         → {p : term V τ}
         → snd (q ⊗ p) βη-≡ p
     pair : ∀{σ τ}
          → {c : term V (σ X τ)}
          → ( fst c ⊗ snd c  ) βη-≡ c
